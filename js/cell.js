// Jake Rosenfeld 4/10/17

// Speed should be in pixels / second
function Cell( /* Arena */ arena, /* Vec2 */ location, /* number */ radius, /* Vec2 */ speed) {
	this.arena = arena;
	this.location = location;
	this.radius = radius;
	this.speed = speed;
}

(function() {

	Cell.prototype.tick = function( /* number */ millisElapsed) {
		// Should never hit this error condition as we don't move unless we are assured to stay in the arena
		if (this.location.distanceTo(new Vec2(0, 0)) > (this.arena.radius - this.radius)) {
			console.log("ERROR: STARTED OUTSIDE ARENA");
			this.location = this.location.normalize().timesScalar(this.arena.getRadius() - (2 * this.radius));
		}

		const movement = this.speed.timesScalar(millisElapsed / 1000);
		const newLoc = this.location.plus(movement);
		const distanceToEdge = (this.arena.getRadius() - this.radius) - newLoc.distanceTo(new Vec2(0, 0));

		if (distanceToEdge > 0) {
			this.location = newLoc;
		} else {
			this.speed = this.speed.reflect(this.location).timesScalar(-1);
			// move next round
		}
	}

	Cell.prototype.draw = function( /* CanvasGraphics */ canvasGraphics, /* Color */ fillOverride = null ) {
		if (fillOverride) {
			canvasGraphics.pushFillColor(fillOverride);
		}

		canvasGraphics.drawCircle(this.location, this.radius, 2);

		if (fillOverride) {
			canvasGraphics.popFillColor(fillOverride);
		}
	}

}());

